# OData proxy

## proxy.json documentation

- `target: string` - Backend server address, use http protocol, because https usually doesn't work.
- `_config: object` - The object containing the username and password that will be calculated for the basic http authentication and added to the request headers.
It is possible to use the `auth: "user:pass"` parameter, which is supported by the library, but it may not work in some cases (see [this](https://github.com/http-party/node-http-proxy/issues/1088) and [this](https://github.com/http-party/node-http-proxy/issues/995)).

Usage:

```
_config: {
    username: string
    password: string
}
```

- `secure: boolean` - Disables SSL, must be false.
- `changeOrigin: boolean` - It's recommended to set to true, because without this option the browser will not ask for authorization if it is not specified in the .json file.

