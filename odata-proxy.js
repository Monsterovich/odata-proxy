#!/usr/bin/env node

const { execPath, exit } = require('process');

const colors = {
    reset: '\x1b[0m',
    blue: '\x1b[34m',
    red: '\x1b[31m',
    green: '\x1b[32m'
};

var routing = null;

const express = require('express'),
    httpProxy = require('http-proxy'),
    fs = require('fs'),
    process = require('process'),
    proxy = new httpProxy.createProxyServer();

function getRoute(req) {
    const dirname = req.url.replace(/^\/([^\/]*).*$/, '$1');
    return routing[dirname];
}

proxy.on('error', function (err, req, res) {
    const route = getRoute(req);
    console.log(`${colors.blue}PROXY ${colors.green}${req.method}${colors.reset}: ${route.target}${req.url} - ${colors.red}${err}${colors.reset}`);
});

const allowCrossDomain = function(req, res) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Credentials', 'true');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'X-Requested-With, Accept, Origin, Referer, User-Agent, Content-Type, Authorization, X-Mindflash-SessionID');

    // intercept OPTIONS method
    if ('OPTIONS' === req.method) {
        res.header(200);
    } else {
        const route = getRoute(req);
        const username = route?._config?.username;
        const password = route?._config?.password;

        if (!route?.headers?.Authorization && username && password) {
            const base64encodedData = Buffer.from(`${username}:${password}`).toString('base64');
            const authString = `Basic ${base64encodedData}`;

            if (route.headers) {
                route.headers.Authorization = authString;
            } else {
                route.headers = {
                    Authorization: authString
                };
            }
        }

        console.log(`${colors.blue}PROXY ${colors.green}${req.method}${colors.reset}: ${route.target}${req.url}`);

        try {
            proxy.web(req, res, route);
        } catch (error) {
            console.log(`Error: ${error}`);
        }
    }
};



const jsonPath = process.argv[2];
let port = process.argv[3];

if (!jsonPath) {
    console.log('Usage: odata_proxy.js <odata.json> [port]');
    exit(0);
}

if (!port) {
    port = 8005;
}

let fileBuffer = null;

try {
    fileBuffer = fs.readFileSync(jsonPath);
} catch (error) {
    console.log(`Cannot open .json file ${jsonPath}:`);
    console.log(error);
    exit(1);
}

try {
    routing = JSON.parse(fileBuffer);
} catch (error) {
    console.log(`Cannot parse .json file ${jsonPath}:`);
    console.log(error);
    exit(1);
}

const app = express();
app.use(allowCrossDomain);
app.listen(parseInt(port));
console.log(`Proxy started on http://localhost:${port}`);